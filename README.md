# Opencart Excel Yükleme

Opencart 3.0.3 üzerinde denenmiştir. Geliştirme aşamasında olup ürünleri sorunsuz olarak sisteme yüklenmesi yapılmaktadır. 

https://www.kurtulusoz.com.tr adresinden diğer döküman ve dosyaları inceleyebilirsiniz.

# Excel Yüklemesi için kullanacağınız url
- /admin/index.php?route=catalog/excel_upload/index&
- örnek excel dosya içerisinde mevcut
```
cd existing_repo
git remote add origin https://gitlab.com/kurtitasarim/opencart-excel-yuekleme.git
git branch -M main
git push -uf origin main
```


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
